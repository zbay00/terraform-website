variable "bucket_name" {}

variable "aws_region" {
    default = "us-east-1"
}

provider "aws" {
    region     = "${var.aws_region}"
}

resource "aws_s3_bucket" "site" {

  bucket = "${var.bucket_name}"
  acl    = "public-read"
  policy = <<EOF
{
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "PublicReadGetObject",
                "Effect": "Allow",
                "Principal": "*",
                "Action": "s3:GetObject",
                "Resource": "arn:aws:s3:::${var.bucket_name}/*"
            }
        ]
}
EOF

  website {
    index_document = "index.html"
    error_document = "error.html"

    routing_rules = <<EOF
            [{
                "Condition": {
                    "KeyPrefixEquals": "docs/"
                },
                "Redirect": {
                    "ReplaceKeyPrefixWith": "documents/"
                }
            }]
            EOF
  }
}
